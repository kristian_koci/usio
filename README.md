# Welcome to Usio micro guide
This micro guide assumes Ubuntu 14.04 upwards

Create a directory called **usio**, under the path **/home/user/**
**mkdir usio**

Then, install virtualenvwrapper

**sudo apt-get install virtualenvwrapper**

Create a virtual environment

 **mkvirtualenv usio**

Work on this newly created environment **workon usio**

Clone this repo into the **usio** directory **(/home/user/usio)**: [usio test](https://bitbucket.org/kristian_koci/usio/)

Install the requirements **pip install -r requirements.txt**

Then, from the app root directory, run **python app/app.py**

The application has my fixer.io key, You can change it on lines 30 and 48 respectively, on **usio.py** file.

If, for some reason, the app doesn't find data on fixer.io, for whatever reason, You will have a logger warning on console, ie:

**rate for {} not found in rdata**

You can run a few tests I made, from projects root folder, like this:

**nosetests**

It should show some results on the console, the tests are about having a 200 OK responses from the server.

**[program:usio]
command = gunicorn --chdir /home/kristian/usio/usio/app usio:app -b localhost:5000
directory = /home/kristian/usio/usio/app
user = kristian**

I've include this command on the run.sh script. Just change permissions on it, **chmod 755 run.sh**
Then run: ./run.sh

Note: Change user **kristian** , also application path, to whichever user is running the server.

**directory=/home/user/usio/usio/app/**

Then:

Go to https://app.beeinstant.com , sign up, it creates a free account, after that, on the console,
write the command which appears the top-right menu Settings > Account, on the Install > StatsBee section:

**wget -O - https://beeinstant.com/install-statsbee.sh | sudo bash -s <public_key> <private_key>**

NOTE: Replace <public_key> and <private_key> with the keys that appear on the top-right menu.

This is an interesting app which monitors different activities from your computer, right into your browser 
(No harm intended).

Write localhost:5000 on your browser, the app should be running by then.

NOTE: The app has a schedule code on it, it can be modified starting from line 82 on the **usio.py** file. 
However, this schedule affects the correct funcitoning of the app, on the usio's github repo I've read something about specifying if some step isn't complete and the reason for it.

The Historical rates are stored on a file called usio.db and usio.sql

On a sidenote, there is a better API statement from fixer.io, such as timeseries, but timeseries is only available to paid users. So, I just used the complete historical, and filtered it through a subroutine in the python file.
