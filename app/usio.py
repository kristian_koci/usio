from flask import Flask, flash, redirect, render_template, request, session, abort, g
from flask_apscheduler import APScheduler
import os
import json
import schedule
import time
import urllib.request
from urllib.request import urlopen
import sqlite3
import logging
import pandas as pd
import datetime
import numpy as np
import requests
import psycopg2
import io

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
logging.debug('This message should appear on the console')
logging.info('So should this')
logging.warning('And this, too')

def getExchangeRates():
    """ Here we have the function that will retrieve the latest rates from fixer.io """
    rates = []
    response = urlopen('http://data.fixer.io/api/latest?access_key=c2f5070ad78b0748111281f6475c0bdd')
    data = response.read()
    rdata = json.loads(data.decode(), parse_float=float) 
    rates_from_rdata = rdata.get('rates', {})
    for rate_symbol in ['USD', 'GBP', 'HKD', 'AUD', 'JPY', 'SEK', 'NOK']:
        try:
            rates.append(rates_from_rdata[rate_symbol])
        except KeyError:
            logging.warning('rate for {} not found in rdata'.format(rate_symbol)) 
            pass

    return rates
 
def getHistoricRates():
    """ Here we have the function that will retrieve the historical rates from fixer.io, since 1999 """
    rates = []
    response = urlopen('http://data.fixer.io/api/1999-01-01?access_key=c2f5070ad78b0748111281f6475c0bdd')#('http://api.fixer.io/latest')
    data = response.read()
    rdata = json.loads(data.decode(), parse_float=float) 
    rates_from_rdata = rdata.get('rates', {})
    for rate_symbol in ['USD', 'GBP', 'HKD', 'AUD', 'JPY', 'SEK', 'NOK']:
        try:
            rates.append(rates_from_rdata[rate_symbol])
            with open('usio.json', 'w') as outfile:
                json.dump(rdata, outfile)
            history_currency = json.load(open('usio.json'))
            df = pd.read_json(open('usio.json'))
            df
            conn = sqlite3.connect('usio.db')
            df.to_sql('usio', conn, if_exists='replace')
        except KeyError:
            logging.warning('rate for {} not found in rdata'.format(rate_symbol)) 
            pass

    return rates

    def printNewTable():
        db = dqsqlite3.connect('usio.db')
        for row in db.execute('select * from rates'):
            print (row)
    printNewTable()

    def importdb(db):
         conn = sqlite3.connect(db)
         c = conn.cursor()
         c.execute("SELECT * FROM rates;")
         for table in c.fetchall():
             yield list(c.execute('SELECT * from ?;', (table[0],)))

@app.route("/" , methods=['GET', 'POST'])
def index():
    rates = getExchangeRates()   
    return render_template('index.html',**locals()) 

@app.route("/historical", methods=['GET', 'POST'])
def historical():
    date_str = "1999-01-01"
    if datetime.datetime.strptime(date_str,"%Y-%m-%d").weekday()<5:
        rates = getHistoricRates()   
        return render_template('historical.html',**locals()) 

""" Here we have the task scheduler to be runned every day at 12:00 am """

'''schedule.every().monday.at("2:48").do(getExchangeRates)
schedule.every().tuesday.at("2:48").do(getExchangeRates)
schedule.every().wednesday.at("2:48").do(getExchangeRates)
schedule.every().thursday.at("2:48").do(getExchangeRates)
schedule.every().friday.at("2:48").do(getExchangeRates)

while True:
    schedule.run_pending()
    time.sleep(1)'''

 
if __name__ == "__main__":
    app.run()

def do_something():
    pass