from usio import app
import unittest
import nose

class TestLatest(unittest.TestCase):
    def test_fixerio_latest(self):
        """ Here we are testing latest rates fixer.io
        we are ensuring that we will have response 200 on the server side """
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        response = self.test_app.post('/', data={'From': 'http://data.fixer.io/api/latest?access_key=c2f5070ad78b0748111281f6475c0bdd'}) 

        # Assert response is 200 OK.                                           
        self.assertEquals(response.status, "200 OK")

class TestHistorical(unittest.TestCase):
    def test_fixerio_historical(self):
        """ Here we are testing historical rates fixer.io
        we are ensuring that we will have response 200 on the server side """
        # Use Flask's test client for our test.
        self.test_app = app.test_client()

        response = self.test_app.post('/historical', data={'From': 'http://data.fixer.io/api/1999-01-01?access_key=c2f5070ad78b0748111281f6475c0bdd'}) 

        # Assert response is 200 OK.                                           
        self.assertEquals(response.status, "200 OK")

    if __name__ == '__main__':   
        nose.run()